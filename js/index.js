new Vue ({
  el: '#app',
  data: {
    search: '',
    postList : [
      {
        country : 'Malaysia',
        description : 'Malaysia is a Southeast Asian country occupying parts of the Malay Peninsula and the island of Borneo.', 
        continent : 'Asia', 
        president : 'Mahathir bin Mohamad', 
        capital : 'Kuala Lumpur',
        dialcode : '+60',
        flag : 'https://upload.wikimedia.org/wikipedia/commons/6/66/Flag_of_Malaysia.svg'
      },
      {
        country : 'Singapore', 
        description : 'Singapore, an island city-state off southern Malaysia, is a global financial center with a multicultural population.',
        continent : 'Asia', 
        president : 'Halimah Yacob', 
        capital : 'Singapore',
        dialcode : '+65',
        flag : 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/Flag_of_Singapore.svg/250px-Flag_of_Singapore.svg.png'
      },
      {
        country : 'Sweden', 
        description : 'Singapore, an island city-state off southern Malaysia, is a global financial center with a multicultural population.',
        continent : 'Europe', 
        president : 'Halimah Yacob', 
        capital : 'Stockholm',
        dialcode : '+46',
        flag : 'https://upload.wikimedia.org/wikipedia/en/4/4c/Flag_of_Sweden.svg'
      },
    ]
  },
  computed: {
    filteredList() {
      return this.postList.filter(post => {
        return post.country.toLowerCase().includes(this.search.toLowerCase())
      })
    }
  }
})